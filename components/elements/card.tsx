import { Box, BoxProps } from "@chakra-ui/react";
import { css } from "@emotion/react";
import React from "react";
const style = css`
  box-shadow: 0 2px 6px 0 #00000018;
  border-radius: 12px;
`;
const Index = (props: BoxProps) => {
  return (
    <>
      <Box {...props} css={[style, props.css]} />
    </>
  );
};

export default Index;
