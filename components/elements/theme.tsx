import { extendTheme, withDefaultColorScheme } from "@chakra-ui/react";

const colors = {
  primary: {
    500: "#30A6FF",
  },
  text: { primary: "#2E4865", secondary: "#D5E0E8" },
  success: "",
};
const theme = extendTheme(
  withDefaultColorScheme({
    colorScheme: "primary",
    components: ["Button"],
  }),

  {
    components: {
      Text: {
        baseStyle: {
          color: "text.primary",
        },
      },
    },
    fonts: {
      heading: `'Heading Font Name', sans-serif`,
      body: `'Mulish', sans-serif`,
    },
    colors,
  }
);

export default theme;
