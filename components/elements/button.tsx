import styled from "@emotion/styled";
import React, { HTMLProps } from "react";
const Button = styled.div<IProps>`
  padding: 14.5px;
  opacity: ${(props) => (props.disabled ? 0.25 : 1)};
  background-color: ${(props) => (props.outline ? "" : "var(--main-color)")};
  border-color: ${(props) => (!props.outline ? "" : "var(--main-color)")};
  box-shadow: ${(props) =>
    !props.outline ? "" : "0px 0px 0px 1px var(--main-color) inset"};
  color: ${(props) => (props.outline ? "var(--main-color)" : "white")};
  width: 100%;
  border-radius: 6px;
  font-weight: 600;
  text-align: center;
`;

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  outline?: boolean;
  disabled?: boolean;
  css?: any;
}
const Index = ({
  outline = false,
  css,
  onClick = () => {},
  disabled = false,
  ...props
}: IProps) => {
  return (
    <Button
      {...props}
      outline={outline}
      css={css}
      disabled={disabled}
      onClick={(e) => !disabled && onClick(e)}
    />
  );
};

export default Index;
