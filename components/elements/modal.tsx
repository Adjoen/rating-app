import { Box, forwardRef } from "@chakra-ui/react";
import styled from "@emotion/styled";
import { keyframes } from "@emotion/react";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useImperativeHandle, useState } from "react";

export type ModalHandle = {
  show: () => void;
};

const showKeyframes = keyframes`
        from { bottom: -100vh; }
        to { left: 0; }
    `;

const OverlayBox = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #00000050;
  z-index: 999;
  .container {
    bottom: 0;
    position: absolute;
    overflow: auto;
    width: 100%;
    border-radius: 20px 20px 0 0;
    background-color: white;
    padding-top: 17px;
    animation-name: ${showKeyframes};
    animation-delay: 0s;
    animation-duration: 0.5s;
    animation-fill-mode: forwards;
    .closeButton {
      width: 20px;
      height: 20px;
      margin-left: auto;
      margin-right: 17px;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .content {
      max-height: 90vh;
      overflow: auto;
      padding: 0 17px 17px 17px;
    }
  }
`;

const Index = forwardRef(
  (
    { children, onClose }: { children: JSX.Element; onClose?: Function },
    ref
  ) => {
    const [visible, setVisible] = useState(false);

    useImperativeHandle(
      ref,
      () => ({
        show,
        close,
      }),
      []
    );

    const close = () => {
      setVisible(false);
      if (typeof onClose === "function") {
        onClose();
      }
    };
    const show = () => {
      setVisible(true);
    };

    return visible ? (
      <OverlayBox onClick={close}>
        <div className="container" onClick={(e) => e.stopPropagation()}>
          <div className="closeButton">
            <FontAwesomeIcon
              icon={faTimes}
              color="#C9D4DB"
              onClick={(e) => {
                e.stopPropagation();
                close();
              }}
            />
          </div>
          <div className="content">{children}</div>
        </div>
      </OverlayBox>
    ) : (
      <></>
    );
  }
);

export default Index;
