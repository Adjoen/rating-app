import { Box, Flex, Spinner } from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useContext, useEffect } from "react";
import { faTimes, faFlag, faStar } from "@fortawesome/free-solid-svg-icons";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { useQuery } from "react-query";
import { Reviews } from "../../../pages/api/reviews";
import moment from "moment";
import { isEmpty } from "../../../utils";
import { Card } from "../../elements";
import { ReviewsContext } from "../../../pages/reviews.context";
import { css } from "@emotion/react";
import styled from "@emotion/styled";
const getData = async () => {
  const URL = `http://localhost:3000/api/reviews`;
  const response = await fetch(URL);
  if (!response.ok) {
    throw new Error("Fetching Error");
  }
  return await response.json();
};

const Container = styled.div`
  margin-bottom: 41px;
`;
const TitleContainer = styled.div`
  margin-bottom: 14px;
  display: flex;
  align-items: center;
  p {
    width: 100%;
    font-weight: 600;
    font-size: 15px;
  }
  .closeButton {
    height: 20px;
    width: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
    svg {
      color: #c9d4db;
    }
  }
`;

const SliderContainer = styled.div`
  margin: 0 -17px;
  .swiper {
    padding: 0 17px;
    padding-bottom: 6px;
    .swiper-slide {
      width: 90%;
      margin-right: 12px;
      :last-child {
        width: 100%;
        margin-right: 0;
      }
    }
  }
`;

function Index({ submitRate }: { submitRate: Function }) {
  const { data, isError, isLoading, isSuccess, status, isFetching } = useQuery(
    "reviews",
    () => getData(),
    {
      // staleTime: 3000, // ms
      refetchInterval: 15000,
      // initialData: initialPrice,
    }
  );

  const { state, dispatch } = useContext(ReviewsContext);

  useEffect(() => {
    if (status === "success") {
      dispatch({ type: "create", data });
    }
  }, [data, status]);

  return (
    <Container>
      {isLoading ? (
        <Box py="14">
          <span>Loading Reviews</span>
        </Box>
      ) : (
        <>
          {isSuccess && !isEmpty(state) && (
            <>
              <TitleContainer>
                <p>Rate Your Latest Transaction </p>
                <div className="closeButton">
                  {isFetching ? (
                    <Spinner size="sm" />
                  ) : (
                    <FontAwesomeIcon icon={faTimes} />
                  )}
                </div>
              </TitleContainer>
              <SliderContainer>
                <Swiper slidesPerView={"auto"}>
                  {state.map((x: Reviews, i: number) => (
                    <SwiperSlide key={i}>
                      <Item
                        data={x}
                        submit={submitRate}
                        isFetching={isFetching}
                      />
                    </SwiperSlide>
                  ))}
                </Swiper>
              </SliderContainer>
            </>
          )}
          {isError && <></>}
        </>
      )}
    </Container>
  );
}

const CardStyled = styled(Card)<{ isFetching: boolean }>`
  opacity: ${(props) => (props.isFetching ? 0.25 : 1)};
  padding: 16px 16px 22px 16px;
  .top {
    margin-bottom: 12px;
    display: flex;
    align-items: center;
    p {
      font-size: 12px;
      :first-child {
        color: var(--text-success-color);
        font-weight: 600;
        width: 100%;
      }
      :last-child {
        text-align: right;
        color: var(--text-secondary-color);
        white-space: nowrap;
      }
    }
  }
  .identity {
    margin-bottom: 14px;
    display: flex;
    align-items: center;
    font-size: 14px;
    .flag {
      height: 24px;
      width: 24px;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-shrink: 0;
      margin-right: 10px;
    }
    .name {
      width: 100%;
    }
    .amount {
      font-weight: 600;
      text-align: right;
      white-space: nowrap;
    }
  }
  .rate {
    p {
      text-align: center;
      font-weight: 600;
      margin-bottom: 10px;
      font-size: 14px;
    }
    .stars {
      display: flex;
      justify-content: center;
      gap: 18px;
      .star {
        height: 20px;
        width: 20px;
        color: #c9d4db;
        svg {
          width: 100%;
          height: 100%;
        }
      }
    }
  }
`;

const Item = ({
  data,
  submit,
  isFetching,
}: {
  data: Reviews;
  submit: Function;
  isFetching: boolean;
}) => {
  const onRateChange = (rate = 0) => {
    if (!isFetching) {
      submit({ ...data, ...{ rate } });
    }
  };

  return (
    <CardStyled onClick={() => onRateChange()} isFetching={isFetching}>
      <div className="top">
        <p>Transaction Success</p>
        <p>{moment(data.createdAt).format("DD MMM")}</p>
      </div>
      <div className="identity">
        <div className="flag">
          <img src={data.flag} />
        </div>
        <div className="name">{data.first_name}</div>
        <div className="amount">
          {data.currency} {data.amount}
        </div>
      </div>
      <div className="rate">
        <p>How's your transaction experience</p>
        <div className="stars">
          {new Array(5).fill("").map((x, i) => (
            <div className="star" key={i}>
              <FontAwesomeIcon
                onClick={(e) => {
                  e.stopPropagation();
                  onRateChange(i + 1);
                }}
                icon={faStar}
              />
            </div>
          ))}
        </div>
      </div>
    </CardStyled>
  );
};

export default Index;
