import styled from "@emotion/styled";
import React from "react";

const Container = styled.div`
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: end;
  div {
    height: 20px;
    width: 20px;
    margin-right: 20px;
  }
`;
const Index = () => {
  return (
    <Container>
      <div>
        <img src="/notification.png" />
      </div>
    </Container>
  );
};

export default Index;
