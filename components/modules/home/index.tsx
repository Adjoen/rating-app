import styled from "@emotion/styled";
import type { NextPage } from "next";
import React, { useRef } from "react";
import { useQuery } from "react-query";
import { Reviews } from "../../../pages/api/reviews";
import Banner from "./banner";
import Features from "./features";
import ModalReview, { ModalReviewHandle } from "./modalReview";
import Notification from "./notification";
import RateTransaction from "./rateTransactions";

export { ReviewsContext } from "../../../pages/reviews.context";

const Home: NextPage = () => {
  const modalReviewRef = useRef<ModalReviewHandle | any>();

  const Container = styled.div`
    padding: 0 17px;
  `;

  return (
    <>
      <Notification />
      <Banner />
      <Container>
        <FeaturesMemo />
        <RateTransaction
          submitRate={(e: Reviews) => modalReviewRef.current.show(e)}
        />
      </Container>
      <ModalReview ref={modalReviewRef} />
    </>
  );
};

const FeaturesMemo = React.memo(Features);
export default Home;
