import styled from "@emotion/styled";
import React from "react";
const Container = styled.div`
  height: 200px;
  display: flex;
  align-items: center;
  div {
    margin: 0 auto;
    text-align: center;
    p {
      font-weight: 600;
      font-size: 21px;
    }
  }
`;

const PaginationContainer = styled.div`
  height: 20px;
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Dot = styled.div`
  height: 5px;
  width: 5px;
  border-radius: 5px;
  margin: 0 2px;
  background-color: ${({ active }: { active: boolean }) =>
    active ? "#8295B5" : "#D5E0E8"};
`;

const Index = () => {
  return (
    <>
      <Container>
        <div>
          <p>Banner</p>
        </div>
      </Container>
      <PaginationContainer>
        {new Array(4).fill("").map((x, i) => {
          const active = i === 2;
          return <Dot active={active} key={i} />;
        })}
      </PaginationContainer>
    </>
  );
};

export default Index;
