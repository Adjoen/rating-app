import { Box, Center, Flex, Spinner, Textarea } from "@chakra-ui/react";
import styled from "@emotion/styled";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import React, {
  forwardRef,
  useContext,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import { useController, UseControllerProps, useForm } from "react-hook-form";
import { COLORS } from "../../../configs/theme";
import { Reviews } from "../../../pages/api/reviews";
import { Button, Modal } from "../../elements";
import { ModalHandle } from "../../elements/modal";
import { ReviewsContext } from "../../../pages/reviews.context";

interface ModalReviewProps {
  children?: React.ReactNode | null;
}
export type ModalReviewHandle = {
  show: (args: Reviews) => void;
} | null;

interface Forms {
  id: string;
  rate: number;
  reviews: number[];
  feedback: string;
}

interface RateControllerType extends UseControllerProps<Forms> {
  reset: Function;
}
interface ReviewsControllerType extends UseControllerProps<Forms> {
  rate: number;
}

const reviews = {
  good: [
    {
      id: 1,
      text: "Money arrived faster",
    },
    {
      id: 2,
      text: "Competitive rate",
    },
    {
      id: 3,
      min: 4,
      text: "Easy to use",
    },
    {
      id: 4,
      text: "Clear Information",
    },
    {
      id: 5,
      text: "Excelent customer service",
    },
  ],
  netral: [
    {
      id: 1,
      text: "Money should arrived faster",
    },
    {
      id: 2,
      text: "Exchanged rate",
    },
    {
      id: 3,
      text: "Add more destinations",
    },
    {
      id: 4,
      text: "Customer service",
    },
    {
      id: 5,
      text: "Clear Information",
    },
    {
      id: 6,
      text: "Easy to use",
    },
  ],
  bad: [
    {
      id: 1,
      text: "Money arrived late",
    },
    {
      id: 2,
      text: "Hard to use",
    },
    {
      id: 3,
      text: "Money didn't arrived",
    },
    {
      id: 4,
      text: "Bad customer service",
    },
    {
      id: 5,
      text: "informations not clear",
    },
    {
      id: 6,
      text: "Expensive rate",
    },
  ],
};

const RateContainer = styled.div`
  .title {
    text-align: center;
    font-size: 15px;
    margin-bottom: 24px;
    font-weight: 600;
  }
  .illustration {
    height: 150px;
    width: 150px;
    margin-bottom: 19px;
  }
  .stars {
    margin-bottom: 43px;
    display: flex;
    gap: 18px;
    justify-content: center;
  }
`;

const StarStyled = styled.div`
  .star {
    height: 30px;
    width: 30px;
    color: ${({ active }: { active: boolean }) =>
      active ? "#FECA14" : "#C9D4DB"};
  }
`;

function Rate(props: RateControllerType) {
  const { field } = useController(props);
  const illustrations = [
    {
      image: "/sad.png",
    },
    {
      image: "/sad.png",
    },
    {
      image: "/netral.png",
    },
    {
      image: "/happy.png",
    },
    {
      image: "/happy.png",
    },
  ];
  return (
    <RateContainer>
      <p className="title">How's your transaction experience?</p>

      {field.value > 0 && (
        <Center>
          <img
            src={illustrations[(field.value as number) - 1].image}
            className="illustration"
          />
        </Center>
      )}
      <div className="stars">
        {new Array(5).fill("").map((x, i: number) => (
          <StarStyled active={field.value >= i + 1} key={i}>
            <FontAwesomeIcon
              className="star"
              key={i}
              icon={faStar}
              onClick={() => {
                const value = i + 1;
                if (value !== field.value) {
                  props.reset({ rate: i + 1 });
                }
              }}
            />
          </StarStyled>
        ))}
      </div>
    </RateContainer>
  );
}
const ReviewsContainer = styled.div`
  margin-bottom: 26px;
  .title {
    font-weight: 600;
    font-size: 15px;
    margin-bottom: 16px;
  }
  .error {
    font-size: 12px;
    color: #df6464;
    margin-bottom: 23px;
  }
  .choicesWrapper {
    display: flex;
    flex-wrap: wrap;
  }
`;

const ChoiceButton = styled.div`
  margin-bottom: 14px;
  margin-right: 14px;
  padding: 3px 12px 3px 12px;
  border-radius: 100px;
  box-shadow: 0px 0px 0px 1px
    ${({ isActive }: { isActive: boolean }) =>
      isActive ? COLORS.primary : "#C9D4DB"}
    inset;
  font-size: 13px;
  font-weight: ${({ isActive }: { isActive: boolean }) =>
    isActive ? 600 : 300};
`;

function Reviews(props: ReviewsControllerType) {
  const { rate } = props;
  const { field, fieldState } = useController(props);
  const { value = [], onChange } = field;

  const getReviews = () => {
    if (rate <= 5 && rate >= 4) {
      return reviews.good;
    } else if (rate === 3) {
      return reviews.netral;
    } else {
      return reviews.bad;
    }
  };

  const getTitle = () => {
    if (rate <= 5 && rate >= 4) {
      return "Tell us what makes you satisfied";
    } else if (rate === 3) {
      return "Tell us what can we improved";
    } else {
      return "Tell us what went wrong";
    }
  };

  const addReview = (newValue: number) => {
    const old = [...(value as number[])];
    const isAdded = old.some((x) => x === newValue);
    let result = [];
    if (isAdded) {
      result = old.filter((x) => x !== newValue);
    } else {
      result = [...old, newValue];
    }
    onChange([...result]);
  };

  return (
    <ReviewsContainer>
      <p className="title">{getTitle()}</p>
      {fieldState.invalid && <p className="error">*Required</p>}

      <div className="choicesWrapper">
        {getReviews().map((review, i) => {
          const isActive = (value as number[]).some((id) => id === review.id);
          return (
            <ChoiceButton
              key={i}
              onClick={() => addReview(review.id)}
              isActive={isActive}
            >
              <p>{review.text}</p>
            </ChoiceButton>
          );
        })}
      </div>
    </ReviewsContainer>
  );
}

const Container = styled.div`
  .text-success {
    font-size: 15px;
    font-weight: 700;
    margin-bottom: 12px;
  }

  .identity {
    display: flex;
    align-items: center;
    font-size: 12px;
    margin-left: 15px;
    margin-bottom: 28px;
    .flag {
      height: 32px;
      width: 32px;
      margin-right: 12px;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-shrink: 0;
    }
    .receiver {
      width: 100%;
    }
    .date {
      white-space: nowrap;
    }
  }
  .feedback {
    padding: 12px 16px;
    box-sizing: border-box;
    height: 120px;
    border: 1px solid #c9d4db;
    border-radius: 10px;
    font-size: 13px;
    margin-top: -24px;
    margin-bottom: 16px;
  }
  .submit-button {
    margin-bottom: 12px;
  }

  .finished {
    .illustration {
      margin-top: 100px;
      height: 200px;
      margin-bottom: 41px;
      img {
        height: 100%;
        width: 100%;
      }
    }
    .title {
      text-align: center;
      font-size: 19px;
      font-weight: 600;
      margin-bottom: 16px;
    }
    .caption {
      font-size: 15px;
      text-align: center;
      margin-bottom: 237px;
    }
  }
`;

const Index = forwardRef<ModalReviewHandle, ModalReviewProps>(
  (props: ModalReviewProps, ref) => {
    const [data, setData] = useState<Reviews>();
    const [finished, setFinished] = useState(false);
    const { dispatch } = useContext(ReviewsContext);

    const [submiting, setSubmiting] = useState(false);

    const modalRef = useRef<ModalHandle | any>();

    const {
      setValue,
      reset,
      getValues,
      handleSubmit,
      register,
      control,
      formState: { errors },
    } = useForm<Forms>({
      defaultValues: {
        id: "",
        rate: 1,
        reviews: [],
        feedback: "",
      },
    });

    const { rate } = getValues();

    useImperativeHandle(
      ref,
      () => ({
        show(e: Reviews) {
          resetForm();
          setValue("rate", e.rate);
          setValue("id", e.id);

          setData(e);
          modalRef.current.show();
        },
      }),
      []
    );

    const close = () => {
      reset();
      setFinished(false);
    };

    const onSubmit = async (data: Forms) => {
      setSubmiting(true);
      const URL = `http://localhost:3000/api/reviews`;
      const body = JSON.stringify(data);
      const response = await fetch(URL, {
        method: "PUT",
        body,
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (!response.ok) {
        throw new Error("Fetching Error");
      }
      dispatch({ type: "remove", data: [data] });
      setFinished(true);
      setSubmiting(false);
    };

    const resetForm = (v?: Forms) => {
      reset(v);

      register("rate", { required: true });
      register("reviews", { required: true, minLength: 1 });
      register("id", { required: true });
    };

    return (
      <Modal ref={modalRef} onClose={close}>
        <Container>
          {!finished ? (
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="text-success">Transaction Success</div>
              <div className="identity">
                <div className="flag">
                  <img src={data?.flag} />
                </div>
                <div className="receiver">
                  <p>{data?.full_name}</p>
                  <p className="text-secondary">
                    {data?.currency} {data?.amount}
                  </p>
                </div>
                <p className="date">
                  {moment(data?.createdAt).format("DD MMM")}
                </p>
              </div>

              <Rate
                control={control}
                name="rate"
                reset={(e: any) =>
                  resetForm({ ...e, ...{ id: getValues().id } })
                }
              />

              {rate > 0 && (
                <Reviews control={control} name="reviews" rate={rate} />
              )}

              {rate < 4 && rate > 0 && (
                <Textarea
                  minHeight={120}
                  maxHeight={200}
                  placeholder="Share your feedback"
                  className="feedback"
                  {...register("feedback")}
                />
              )}

              {!submiting ? (
                <>
                  <Button
                    className="submit-button"
                    disabled={rate < 1}
                    onClick={() => handleSubmit(onSubmit)()}
                  >
                    Submit
                  </Button>

                  {rate < 4 && rate > 0 && <Button outline>Chat Us</Button>}
                </>
              ) : (
                <Center>
                  <Spinner />
                </Center>
              )}
            </form>
          ) : (
            <div className="finished">
              <Center>
                <div className="illustration">
                  <img src="/smile.png" style={{ width: "100%" }} />
                </div>
              </Center>
              <div className="title">Thank you, Buddy!</div>
              <div className="caption">
                We appreciate your feedback & what you've shared will help to
                improve our service
              </div>

              <Button onClick={() => modalRef.current.close()}>
                Back to Main Page
              </Button>
            </div>
          )}
        </Container>
      </Modal>
    );
  }
);

export default Index;
