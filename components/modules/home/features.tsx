import { Center } from "@chakra-ui/react";
import { css } from "@emotion/react";
import styled from "@emotion/styled";
import React from "react";
import { Card } from "../../elements";

const ContainerStyle = css`
  padding: 20px 30px;
  display: flex;
  gap: 10px;
  margin-bottom: 24px;
`;

const FeatureContainer = styled.div`
  width: 100%;
  p {
    font-size: 12px;
    line-height: 1.2;
    text-align: center;
    font-weight: 600;
  }
`;

const IconContainer = styled.div`
  height: 36px;
  width: 36px;
  margin-bottom: 8px;
`;

const Index = () => {
  const property = [
    {
      icon: "/send.png",
      text: "Send Money",
    },
    {
      icon: "/chat.png",
      text: "Chat us",
    },
    {
      icon: "/refund.png",
      text: "Refund",
    },
  ];
  return (
    <Card css={ContainerStyle}>
      {property.map((x, i) => (
        <FeatureContainer key={i}>
          <Center>
            <IconContainer>
              <img src={x.icon} />
            </IconContainer>
          </Center>
          <p>{x.text}</p>
        </FeatureContainer>
      ))}
    </Card>
  );
};

export default Index;
