import type { NextApiRequest, NextApiResponse } from "next";

export interface Reviews {
  id: string;
  createdAt?: string;
  first_name: string;
  last_name: string;
  full_name: string;
  currency: string;
  amount?: string;
  flag?: string;
  rate: number;
}
interface ReviewsStar {
  id: string;
  star: number;
}

const initialReviews: Reviews[] = [
  {
    id: "1",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "John",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    amount: "1000",
    flag: "https://flagicons.lipis.dev/flags/4x3/ai.svg",
    rate: 0,
  },
  {
    id: "2",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "Sheilla",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    amount: "2000",
    flag: "https://flagicons.lipis.dev/flags/4x3/bh.svg",
    rate: 0,
  },
  {
    id: "3",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "John",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    flag: "https://flagicons.lipis.dev/flags/4x3/dm.svg",
    amount: "3000",
    rate: 0,
  },
  {
    id: "4",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "John",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    flag: "https://flagicons.lipis.dev/flags/4x3/hn.svg",
    amount: "4000",
    rate: 0,
  },
  {
    id: "5",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "John",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    flag: "https://flagicons.lipis.dev/flags/4x3/hm.svg",
    amount: "5000",
    rate: 0,
  },
  {
    id: "6",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "John",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    flag: "https://flagicons.lipis.dev/flags/4x3/fo.svg",
    amount: "6000",
    rate: 0,
  },
  {
    id: "7",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "John",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    flag: "https://flagicons.lipis.dev/flags/4x3/er.svg",
    amount: "7000",
    rate: 0,
  },
  {
    id: "8",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "John",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    flag: "https://flagicons.lipis.dev/flags/4x3/cu.svg",
    amount: "8000",
    rate: 0,
  },
  {
    id: "9",
    createdAt: "2022-05-30T06:51:17.947Z",
    first_name: "John",
    last_name: "Doe",
    full_name: "John Doe",
    currency: "EUR",
    flag: "https://flagicons.lipis.dev/flags/4x3/bi.svg",
    amount: "9000",
    rate: 0,
  },
];

// temporary db
let data: Reviews[] = initialReviews;

const updateStar = (newReviews: Reviews) => {
  const find = data.map((item) => {
    if (newReviews.id == item.id) {
      item.rate = newReviews.rate;
    }
    return newReviews;
  });
  data = find;
  return [
    {
      id: newReviews.id,
      rate: newReviews.rate,
    },
  ];
};

export default (req: NextApiRequest, res: NextApiResponse) => {
  try {
    let response: Reviews[] | ReviewsStar[] = [];

    switch (req.method) {
      case "GET":
        response = data.filter((x) => !x.rate);
        break;
      case "PUT":
        data = data.filter((x) => x.id != req.body.id);
        response = data;
        break;
    }

    setTimeout(() => {
      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.end(JSON.stringify(response));
    }, 2000);
  } catch (err: any) {
    res.end(JSON.stringify({ message: err.message }));
  }

  // res.statusCode = 400;
  // res.end("Bad Request");
};
