import React, { useReducer } from "react";
import Home from "../components/modules/home";
import { ReviewsContext, ReviewsReducer } from "./reviews.context";

const Index = () => {
  const [stateReviews, dispatchReviews] = useReducer(ReviewsReducer, []);

  return (
    <ReviewsContext.Provider
      value={{ state: stateReviews, dispatch: dispatchReviews }}
    >
      <Home />
    </ReviewsContext.Provider>
  );
};

export default Index;
