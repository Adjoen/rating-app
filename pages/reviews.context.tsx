import { createContext, Dispatch } from "react";
import { Reviews } from "./api/reviews";

export const ReviewsContext = createContext<{
  state: Reviews[];
  dispatch: Dispatch<ReviewsActionType>;
}>({ state: [], dispatch: () => {} });
type ReviewsActionType = {
  type: string;
  data: Reviews[] | any;
};
export const ReviewsReducer = (
  state: Reviews[],
  action: ReviewsActionType
): Reviews[] => {
  switch (action.type) {
    case "create":
      state = action.data;
      return state;

    case "remove":
      const result = state.filter((x) => x.id !== action.data[0].id);

      return [...result];

    default:
      return state;
  }
};
